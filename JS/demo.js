// // ES5
// var firstName = "CyberSoft";

// // var cho phep gan lai gia tri cho 1 bien da co truoc
// firstName= "CeyberSoft1";

// // Viec dat cung ten bien cho phep viec khai bao trung ten bien
// // Khi khai bao lai 1 bien cung ten thi bien do se duoc lay gai tri moi ngay ca khi bi trung
// var firstName = "CeberSoft2";
// // console.log(firstName);

// // ES6
// let secondName = "CeberLearn";
// // let van cho phep trung ta gan lai gia tri
// secondName = "CyberLearn2";
// // let khong cho phep khai bao trung ten bien
// // let secondName = "CyberLearn3";
// // console.log(secondName);

// // const khong cho phep chung ta gan lai gia tri 
// const PI = 3.14;
// // PI = 3.142;
// // const cung khong cho khai bao lai bien
// // const PI = 3.15;
// // console.log(PI);


// /* 
//     Vi du ve hoisting

// */

// // Vi du ES5
// console.log(sum(2,3));
// console.log(city);//loi goi truoc khi khai bao la undifined :chua gan gia tri
// // nhung loi chung ta mong muon la chua duoc khoi tao  
// // Khai bao ham
// function sum(x,y){
//     return x + y;
// }

// // Khai bao bien
// var city = "Ha Noi";

// // Hoisting cho ES6
// // let 

// //  console.log(NAME);
// // console.log(address);
// // console.log(secondCity);

// const NAME = "Cyber Soft";//uninitialized
// let address = "456 Su Van Hanh";//uninitialized : chua khai bao
// var secondCity = "Ho Chi Minh";//undefined : chua gan gia tri nhung da khai bao


// // Scope - Pham vi cua bien

// // khai bao bien global voi var nen chung ta co the dung o bat cu dau chung ta muon
// let name  = "Huyen beo";

// // ham 
// function tinhDoYeuThuongCuaHuyenBeo(){
//     // love la bien local tao bang var nen chung ta chi co the dung bien trong ham nay
//     let love = 3000;
//     console.log(name + " yeu anh " + love + " loves!");
//     if(love > 2999){
//         // trong if thi var nickName van dung duoc
//         // chung ta su dung let
//         let nickName = "Han beo";
//         console.log(nickName + " yeu ba " + love + " loves!");
//     }

//     console.log(nickName + " hoi gay! Rang an them!");
// }


// tinhDoYeuThuongCuaHuyenBeo();
// console.log(nickName);
// console.log(love);

// Arrow function 

// Cac kieu khai bao cho ES5
// function declaration function ten_function(tham so){codes}

// trong ES5 chi co the dung var nhung trong ES6 thi co the dung ca var va let
// function expression var hoac let name = function(tham so){codes}

let hello = function (x, y ){
    return x + y;
}

console.log(hello(5, 6));

// Trong ES6
//let hello1 = (tham so) => {codes}
// trong ES6 arrow function se duoc khai bao theo kieu expression function 
// bo chu funciton va khai bao theo expression function () => {}


let helllo1 = (z, r) => { return z - r};

console.log(helllo1(10, 5));

// cach viet rut gon cua arrow function do la neu nhu chi co 1 tham so truyen vao
// thi chung ta co the rut gon bang cach bo di dau () vi du o duoi

let hello2 = name => { console.log( name + " yeu Anh Tai!")};

hello2("Huyen");

// cach rut gon nhat trong truong hop khi chung ta chi co 1 tham so va trong code chi co
// 1 cau lenh return duy nhat thi chung ta se bo di {}  va tu khoa return va chi de lai gia tri can return

let hello3 = name => name + " nice to meet you!";
console.log(hello3("Tai"));

console.log(typeof(() => "Hello may cung!"));
// de tranh loi khi truyen arrow va call back function thi chung ta nen khai bao arrow theo kieu expression
// gan 1 bien cho arrow function do sau do dung bien do de su dung 
 let hello4 = () => "Hello may cung!";

 console.log(typeof(hello4));
