// Destructuring dung de lay gia tri cua cac phan tu trong 1 mang co san de gan cho cac bien
// duoc tao ra trong cung 1 luc de tranh lap lai code

let programs = ["JavaScript","Python","Java"];

// Cach binh thuong trong ES5

console.log(programs[0]);
console.log(programs[1]);
console.log(programs[2]);

// voi ES5 chung ta phai lap lai code moi lan muon lay ra 1 phan tu 

// Voi ES6 su dung destructuring voi mang

let [first,second,third] = programs;

// cu phap [bien_a,bien_b,bien_c] = mang_muon_lay_gia_tri;
// sau do bien_a se duoc gan voi phan tu dau tien cua mang chung ta muon lay gia tri
// tuong tu voi bien_b , bien_c
console.log(first);
console.log(second);
console.log(third);


// Object

let pet = {
    name:"Be Cun",
    type:"Corgi",
    age:3,
    sex:"male",
    size:{
        weight:'30kg',
        length:'56cm',
        height:'50cm',
    }
}


// su dung voi object
let {name,age,type} = pet;
console.log(name);
console.log(age);
console.log(type);

// khi muon su dung destructuring cho object chung ta cung khai bao cu phap giong nhu mang
// chi khac la[] thay bang {}
// va ten bien phai trung voi ten thuoc tinh cua object do

// neu chung ta muon lay cac thuoc tinh trogn object con size 

let {weight,height} = pet.size;
console.log(weight);
console.log(height);

// de tranh nham lan trong qua trinh xu ly chung ta co the dat ra nhung ten goi khac cho cac bien 

let {weight : w, height : h} = pet.size;
// chung ta da gan cho weight va height 1 cai ten khac la w va h. w va h cung se duoc chua gia tri cua wight va height lay tu 
// pet.size
console.log(w,h);


// Template String
 let petName = "ca";
 let action = "boi";

//  trong ES5 chung ta phai dung dau +
 console.log("Minh la " + petName + ". Viec cua ming la " + action);


//  ES6 dung templateString
// muon truyen 1 bien vao template string chung t su dung ${ten_bien}
console.log(`Minh la ${petName}. Viec cua minh la ${action} `);


let a = 2;
let b = 3;
// doi voi templateString thi khi chung ta truyen dau + vao thi + se duoc hieu la cong 2 so 
// chu khong phai la de noi chuoi;
console.log(`Sum : ${a+b}`);

// su dung de them noi dung cho the HTML
// co the su dung de them bat ky thuoc tinh gi cho cac the
document.getElementById("section1").innerHTML = `
    <div class="class1">
        <p>
            Minh la ${petName}. Viec cua minh la ${action}
        </p>    
    </div>
`;