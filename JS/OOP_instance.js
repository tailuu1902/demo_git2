// Extend
// ES5

function Father(name){
    this.name = name;
}

Father.prototype.creatColorEyes = function(){
    console.log("Black");
}

// chúng ta cần truyền name cho children bởi vì
// khi chúng ta kế thừa thuộc tính name của cha và thuộc tính
// đó nhận giá trị của tham số truyền từ ngoài vào cho nê
// với childrend chúng ta cũng phải truyền giá trị từ ngoài vào 
// cho name nhưng name ở đây là giá trị của Children chứ kp của 
// Father(name);
function Childrend(name){
    // kê thừa thuộc tính từ lớp cha
    Father.apply(this,arguments);
    // this ở đây sẽ là Father và arguments là những thuộc tính của cha 
    // sử dụng hàm apply(this,arguments) để thêm các thuộc tính
}

// kể thừa phương thức từ cha thông qua prototype
// tên_lớp_con.prototype = new tên_lớp_cha;
Childrend.prototype = new Father;
// gọi phương thức sau khi đã kế thừa
Childrend.prototype.creatColorEyes = function(){
    console.log("Brown");
};
// nếu như muốn kế thừa y chang của father thì chúng ta không cần sửa 
// nhưng cũng có thể sửa function như ở trên
// cũng có thêm phương thức mới của con 

Childrend.prototype.createSkinColor =  function(){
    console.log("Yellow");
}

let child = new Childrend("Con ba Tài");
console.log(child.name);
child.creatColorEyes();
child.createSkinColor();

// Sử dụng ES6 để kế thừa từ class cha
// và vẫn có thể thêm nhưng class riêng mà chỉ lớp
// con mới có 
class Father1{
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    creatColorEyes(){
        console.log("Black");
    }
}


// extends ES6

class Children1 extends Father1{
    // sử dụng object literal để khai báo hàm mà ko cần dùng keyword fucntion()
    createSkinColor(){
        console.log("Brown");
    }
}    


Children1.prototype.sex = "Female";

let child1 = new Children1("Hân",18);
console.log(child1.name,child1.age,child1.sex);
child1.createSkinColor();
child1.creatColorEyes();


// Bài 11 OOP Method Overriding Super Proxy

// Method Overriding
// class cha 
class Person{
    constructor(name){
        this.name = name;
    }
}

// Them 1 phương thức

Person.prototype.getName = function(){
    return this.name;
}


// Kể thừa cho class con
class Student extends Person{
    // Nêu chúng ta muốn lấy lại hàm getName() cũ của Person
    // chúng ta có thể dùng super để viết lại hàm getName() của person
    // super ở đây đại diện cho lớp cha mà lớp con kế thừa
    getPersonName(){
        return super.getName();
    }
}

// Ghi đè lại funciton getName của thẻ cha bằng cách sử dụng prototype
Student.prototype.getName = function(){
    return "Hello " + this.name;
}

// check
let st = new Student("Trúc");

console.log(st.getName());
// lấy ra kết quả của hàm getName() của Person
console.log(st.getPersonName());


// Proxy: tác dụng thay đổi thuộc tính của đối tượng hoặc cho mảng hàm or Proxy khác
 
// Ví dụ về Proxy
// Khai báo 1 đối tượng 

let pet = {
    name:"cau vang",
    age:2,
    breed:"shiba",
}

// Sử dụng Proxy để truy xuất đến thuộc tính name và thay đổi giá trị cho name
// Trong Proxy sẽ truyền vào 2 tham số 
// tham số đầu tiên là target: chính là đối tượng mà chúng ta muốn truy xuất thuộc tính
// tham số thứ 2 là 1 hàm trong hàm chứa các phương thức để xử lý các thuộc tính trong 
// target
// Trong proxy có 2 hàm phổ biến được sử dụng là get() để lấy giá trị thuộc tính
// set() dùng để đặt lại giá trị cho các thuộc tính
let pet1 = new Proxy(pet,{
    // Trong Proxy sẽ có 2 tham số 
    // Tham số 1: là đối tượng mà chúng ta sẽ sử dụng để thay đổi thuộc tính
    // Tham số 2: là 1 hàm chứa các phương thức để xử lý các thuộc tính của đổi tượng mà chúng ta đã truyền vào 
    // cho tham số 1

    // hàm get(target, prop, receiver){}
    // tham số target chính là đối tượng mà chúng ta truyền vào ở Proxy và ở đây target = pet
    // tham số prop: là các thuộc tính của đối tượng mà chúng ta muốn thay đổi
    // receiver: đối tượng sau khi xử lý và gắn Proxy
    get(target, prop, receiver){
        // kiểm tra các prop xem prop nào có style bằng string nếu đúng thì chuyển sang toUpperCase();
        // nếu sai thì chỉ cần trả về prop đó
        // thực hiện kiểm tra cho tất cả các prop như 1 vòng loop
        // console.log("a");
        if( typeof target[prop] === 'string'){
            return target[prop].toUpperCase();
        }
        // Cần phải return 1 lần nữa vì return bên trong chỉ trả lại kết quả cho hàm get
        return target[prop];   
        
    }
})
// Thực tế hàm get() chỉ lấy ra giá trị và trả lại ngay cho chúng ta chứ chưa thực sự thay đổi giá trị của các prop của target
// chúng ta có thể thấy trong các câu lệnh console ở dưới
console.log(pet1.name);
console.log(pet1.age);
console.log(pet1.breed);
console.log(pet1);

// Để set lại thuộc tính mới cho các đối tượng thì chúng ta phải sử dụng hàm set()

let pet2 = new Proxy(pet,{
    // hàm set sẽ nhận vào 3 tham số 
    // target chính là đối tượng mà chúng ta truyền vào cho tham số 1 của Proxy
    // prop chính là các thuộc tính của target pet 
    // value là giá trị mới mà chúng ta muốn gán cho các prop của target pet
    set(target, prop, value){
        // kiểm tra xem prop có phải là age không + kiểu giá trị truyền vào cho prop có phải là number hya không
        if(typeof value !== 'number' && prop === "age"){
            // thông báo ra lỗi 
            throw new TypeError('Age must be number.');
        }
            
        target[prop] = value;//cập nhật giá trị cho prop age
        return true;
        // hàm set sẽ trả về 1 kết quả boolean nếu true thì đã set thành công còn false là ko set thành công
        // do giá trị đã được cập nhật trong quá trình xử lý 
    }
})

// Khi truyền vào cho age 1 biến là số thì chúng ta sẽ nhận lại 1 lỗi"Age must be number."
// pet2.age = "123";
pet2.age = 123;

console.log(pet2.age);