// this
// ES5
// khai bao 1 lop doi tuong nhanh
let hocSinhSinhVien = {
    hoTen: "Hoc Sinh 1",
    lop: "Ngu Van",
    diemThi:[10,9,8],
    // laythongtinsinhvien: function(){
    //     console.log("Ho ten " + this.hoTen + " lop - " + this.lop);
    //     // this nam trong ham nao thi this chinh la ham do (ngu canh)
    //     // ma laythongtinsinhvien la method of doi tuong hosSinhSinhVien => this chinh la 1 hoc vien
    //     // vi vay khi dung this o ngoai goi den diemThi thi no van dung vi diemThi la 1 thanh phan cua hoc vien

    //     // C1 bien tam 
    //     // de xu ly van de ban trong ham map() chung ta co the su dung bien tam
    //     var _bind = this;//this o day chinh la doi tuong hocSinhSinhVien se luu cho _bind

    //     this.diemThi.map(function(diemThi, index){
    //         // diemThi se la tung phan tu trong map diemThi va index la chi muc cua tung phan tu trong array do
    //         console.log("Ho ten " + _bind.hoTen + " lop - " + _bind.lop);
    //         //  su dung bien tam de luu tru this vi luc do this la hocSinhSinhVien cho nen no se khong bi hieu
    //         // nham khi chung ta goi this trong function trong map
    //         console.log("Diem thi thu: " + (index + 1) + " - diem: " + diemThi);
    //         // trong truong hop nay map()thuc hien nhien vu cua 1 vong lap cho phep chung ta duyet qua mang 
    //         // map() nhan vao 1 function va trong function do se co 2 tham so 1 la dai dien cho cac phan tu torng mang
    //         // index dai dien cho chi so cua cac thanh phan trong mang 
    //         // trong truong hop nay this se la ham call back function cua ham map()
    //         // this o day se khong con la hocVien nua nen chung ta se khong truy xuat duoc den hoTen va Lop
    //         // trong nay this la function(diemThi, index)-function an danh

    //     })
    //},
    // laythongtinsinhvien: function(){
    //     console.log("Ho ten " + this.hoTen + " lop - " + this.lop);
    //     // C2 su dung ham bind cho phep chung ta chuyen doi ngu canh de this tro ve la ham  ngoai 
    //     this.diemThi.map(function(diemThi, index){
    //         console.log("Ho ten " + this.hoTen + " lop - " + this.lop);
    //         console.log("Diem thi thu: " + (index + 1) + " - diem: " + diemThi);          
    //         }.bind(this))
    //         // o day chung ta se su dung ham bind(this) cho ham function an danh trong map 
    //         // de dinh nghia lai ngu canh cho this luc nay this se duoc dinh nghia lai la function 
    //         // laythongtinsinhvien o ngoai cho this ra ngoai ham goi den ham bind
    // },
    // 2 cach cho ES5

    // Su dung ES6
    laythongtinsinhvien: function(){
        // neu nhu ham laythongtinsinhvien chung ta gan cho arrow function thi con tro this ben trong 
        // se khong tim duoc bat cu 1 ngu canh(function()) nao o ngoai cho nen this se khong biet minh thuoc
        // ham nao vi arrow function khong dinh nghia ngu canh cho this 
        console.log("Ho ten " + this.hoTen + " lop - " + this.lop);
        // Su dung arrow funciton cho ES6
        // do arrow function khong dinh nghia ngu canh cho chinh no tuc la no tu choi this 
        // cho nen khi chung ta su dung this trong arrow function thi no se tu dong tim 1 function 
        // o ngoai arrow function de lam ngu canh cho no vi vay trong truong hop nay this se tim 
        // function laythontinsinhvien lam ngu canh cho no vi vay this la doi tuong hocSinhSinhVien(co keyword function) cho nen 
        // no co the goi duoc den hoTen va lop
        this.diemThi.map((diemThi, index) => {
            console.log("Ho ten " + this.hoTen + " lop - " + this.lop);
            console.log("Diem thi thu: " + (index + 1) + " - diem: " + diemThi);          
            });
          
    },
    // => tong ket khi chung ta khoi tao function cho 1 doi tuong thi chung ta se su dung function() nhu binh thuong
    // con khi ap dung cho nhung ham khac ben trong function() do thi chung ta se dung arrow function thi no
    // se mac dinh lay ngu canh(function()) o ngoai cua doi tuong lam ngu canh cho chinh no de tranh viec nham
    // lan ngu canh cua chinh no 
} 

hocSinhSinhVien.laythongtinsinhvien();
// => this chinh la doi tuong hocSinhSinhVien


hello();

let hello = () => {
    console.log("Hello everyone");
}