// OOP trong ES5
// class:lớp đối tượng
// cách đặt tên class: camel(lạc đà)
// viết hoa chữ cái đầu tiên của từ thứ 2 
// hoặc sử dụng pascal camel: GetInfo
function Student(name, address){
    this.name = name;
    this.address = address;                                                                                                                                                                                                 
}

// Khai báo 1 instance của lớp đối tượng 
let student = new Student("Hoc viên 1", "465 Sư Vạn Hạnh");

console.log(student.name);
console.log(student.address);

// ES6 không cho phép đặt trùng tên class
// sử dụng key word class
class Student2{
    // Trong ES6 chúng ta sẽ có constructor()
    // hàm để khởi tạo những thuộc tính ban đầu của đối tượng 
    constructor(name, address){
        this.name = name;
        this.address = address;
    }
}

// chúng ta cũng cần tạo 1 instance giống như ES5

let student1 = new Student2("Học viên 2", "789 Sư Vạn Hạnh");

console.log(student1.name, student1.address);