// Object Literals
// cach truyen gia tri cho 1 thuoc tinh thong thuong cua object
// ES5: chung ta co the truyen vao cho thuoc tinh 1 bien co cung ten voi thuoc tinh do
let name = "Huyen";
let myObject ={
    name : name,
    sayHi : function(){
        console.log("Hello, my name is " + this.name);
    }
}

myObject.sayHi();

// ES6 
// Cach rut gon 1(theo Object Literal)
// neu chung ta co ten bien va ten thuoc tinh cua object 
// cung ten voi nhau thi khi khai bao thuoc tinh do voi
// gia tri cua bien trung ten chung ta chi can viet rieng
// ten cua thuoc tinh
let myObject2 ={
    // chi can viet moi name vi ten thuoc tinh trung voi ten bien
    name,
    // khong can viet tu khoa function khi khai bao 1 phuong thuc cho object
    sayHi2(){
        console.log("Hello, my name is " + this.name);
    }
}

myObject2.sayHi2();

// Cach 2 object literal cho function 
// chung ta chi can viet ten function ma khong can tu khoa function()
// va chung ta cung khong dung arrow function khi khai bao 
// 1 phuonng thuc cho object


// ngoai ra chung ta cung co the truyen 1 ten dong cho thuoc tinh 

let name2 = "ten";
let myObject3 ={
    // Khi chung ta dat name2 trong [] thi tuc la chung ta dang truyen cho
    // name 1 ten động ở đây khi chúng ta muốn truy xuất đến giá rị
    // của thuộc tính name2 chúng se không chấm đến name2 mà chúng ta 
    // sẽ chấm đến giá trị của tên biến mà chúng ta đã truyền vào làm tên thuộc tính
    // ten: "Tai",
    [name2] : "Tai",
    sayHi3 : function(){
        console.log("Hello, my name is " + this.ten);
    }
}

myObject3.sayHi3();


// for 
let currents = ["VND",,"USD","SGN"];
for(let i =0 ;i< currents.length;i++){
    console.log(currents[i]);
}
// voi for binh thuong neu co empty element thi for se lay luon gia tri do
// va tra ve undefined vi vay chung ta can phai ktra gia tri cua moi phan tu 
// ES5 
// for ... in sử dụng như for bthg và dùng index để lấy ra 
// các giá trị
// for .. in thi khong lay empty element ma bo qua luon nen cta khong phai kiem tra
for( let i in currents){
    console.log(i , currents[i]);
}

// ES6 
// for ... of
// Nhu ham for binh thuong lay luon ca gia tri empty nen phai ktra
// dùng trong trường hợp chỉ cần lặp qua các phần tử của mảng
// mà không cần dùng đến index
for(let a of currents){
    console.log(a);
}

// Nếu chúng ta muốn sử dụng for ... of nhưng vẫn lấy được giá trị
// của index thì chúng ta phải sử dụng hàm entries() của mảng
for(let [index, value] of currents.entries()){
    console.log( index , value);
}
// ở đây chúng ta sẽ tạo ra 1 spread kiểu mảng có index và value 
// hàm entries sẽ trả lại 1 mảng với 2 phần từ là index và giá trị của phần từ 
// sau mỗi lần lặp 