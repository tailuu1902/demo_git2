function sum(x,y){
    return x + y;
}

function sum2(x,y,z){
    return x + y + z;
}
// // cau lenh dung de export 1 ham ra khoi 1 file theo nodeJS
// module.exports = sum;

// Export theo ES6

// export default
// nhieu ham cung tuong tu ham1,ham2
// export default sum,sum2;

// export khong co default
// export nhieu ham bang cach ham1,ham2
export {sum,sum2};