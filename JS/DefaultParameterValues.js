// function lay thong tin 
// function getInfo(name, age){
    // trong ES5 cta phai kiem tra xem gia tri truyen vao co undefined hay khong
    // C1: dung if ... else
    // if( name == undefined || age == undefined){
    //     console.log("Gia tri khong hop le!");
    //     name = "Default value!";
    //     age = 18;
    // }

    // C2; dung ternary operator
    // dieu kien 1 == dien kien 2 + toan tu so sanh (?) + ket qua neu dung : ket qua neu sai
    // phai co 1 bien de nhan lai gia tri tra ve tu ternary operator
    // name = name == undefined ? "default name" : name;

    // age = age == undefined ? 18 : age;
    // C3 su dung toan tu ||
    // neu name = undefined, NaN, null thi no se tu dong lay gia tri ben phai 
    // name = name || "deafault name";
    // age = age || 31 ;
//     if(age >= 18 && age <= 30){
//         console.log( name + " dang con tre lam ban oi! Moi " + age + " thoi");
//     }else{
//         console.log( name + " da " + age  + " roi do! Ban dang qua nho tuoi hoac da lon tuoi roi do !");
//     }
// }


// cho ES6

let getInfo = (name = "Mi", age = "18") => {
    // khai bao default parameter values ket hop voi arrow function trong ES6
    if(age >= 18 && age <= 30){
        console.log( name + " dang con tre lam ban oi! Moi " + age + " thoi");
    }else{
        console.log( name + " da " + age  + " roi do! Ban dang qua nho tuoi hoac da lon tuoi roi do !");
    }
}
// o ES 6 chung ta co the khai bao default parameter  value ngay khi khai bao ham khi su dung arrow function
// cho nen khi goi ham chung ta ko can phai truyen gai tri vao cho tung tham so cung duoc va cung khong
// can kiem tra undefined cho nhung tham so cua chung ta 
getInfo();