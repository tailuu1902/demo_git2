// rest parameters
// khai bao parameter
// diem se la 1 mang de truyen vao 
let tinhDiemTB = (...diem)=>{
    let tongDiem =0;
    //diemTP se la cac phan tu trong mang diem ma chung ta da truyen vao 
    diem.map((diemTP, index)=>{
        tongDiem += diemTP;
    })
    // diem.reduce((diemTong, diemHienTai, index)=>{
    //     diemTong+=diemHienTai;
    //     console.log(diemTong);
    //     // console.log(diemHienTai);

    // })
    console.log(tongDiem/3);
}

tinhDiemTB(8,9,10);


// Spread Operator

// Them phan tu vao trong mang hoac them thuoc tinh vao object

let mangC = [1,2,3,4];
// Khi chung ta muon them tat ca cac phan tu cua mang C vao mang D 
// khi khai bao mang D chung ta chi can khai bao  [...mangC]
// ...mangC khi nay la 1 spread va no se lay tung phan tu trong mangC vao mang D
// ...mangC = 1,2,3,4 => [...mangC] = [1,2,3,4]; 
let mangD = [...mangC,7];
// chung ta cung co the khai bao 1 spread voi cac tham so khac nhu bthg 
// Neu muon them phan tu vao mangD chung ta co the su dung phuong thuc push
// Thuong duoc dung de copy 1 mang ra 1 mang moi 
mangD.push(6);
console.log(mangD);


// Them thuoc tinh cho doi tuong

let pet = {
    name:"Be Na",
    type:"Snake",
}

// doi tuong moi 
// dung de cop nhung thuoc tinh cua doi tuong cu sang doi tuong moi

let newPet = {
    ...pet,
    age:1,
}

console.log(newPet);