
// console.log(sum(5,7));

// NodeJS

// su dung cau lenh require de import nhung gi ma module sum export
// o day la 1 function()
// su dung cau lenh require(path_cua_module_can_export)
// var callSum = require('./sum');

// console.log("Cong 2 so: " + callSum(5,2)); 

// ES6
// C1: trong ham main chung ta co the su dung import de nhan vao bat cu thu gi ma ben export tra lai
// trong truong hop nay la 1 funciton va chung ta co the dat ten bat cu cho bien nhan lai function nay
// from + path_module_can_import (nho phai co .js)
// import callSum from './sum.js';
// console.log("Cong 2 so: " + callSum(5,2)); 

// khi muon import ket qua duoc export khong co dafault chung ta phai import ten ham or bien giong het 
// nhu ben export
// import {sum} from './sum.js';
// console.log("Cong 2 so: " + sum(9,10));

// neu chung ta muon doi ten ham nhan vao chung ta su dung tu khoa as
// bay h sum se co ten SUM
// ben export co bao nhieu ham thi ben import se nhan bay nhieu ham
// import {sum as SUM} from './sum.js';
// console.log("Cong 2 so: " + SUM(9,10));

import {sum,sum2} from './sum.js';
console.log("Cong 2 so: " + sum(9,10));
console.log("Cong 3 so: " + sum2(8,9,10));

// Neu chung ta muon lay all cac function or bien duoc export tu file export chung ta su dung

import * as  sumFunctions from './sum.js';

// KHi chung ta muon su dung cac function duoc export thi chung tase goi qua sumFunctions
console.log("Cong 2 so: " + sumFunctions.sum(9,9));
console.log("Cong 3 so: " + sumFunctions.sum2(9,10,11));


console.log(132);